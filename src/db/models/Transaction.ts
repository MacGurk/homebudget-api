import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import User from './User';
import { TransactionResponse } from '../../api/transaction';

export default class Transaction extends Model {
  public id!: string;
  public date: Date;
  public User: User;
  public description: string;
  public price: number;
  public settled: boolean;

  static _init(sequelize: Sequelize): void {
    Transaction.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        date: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        price: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        settled: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
        modelName: 'Transaction',
      },
    );
  }

  getResponse(): TransactionResponse {
    return {
      id: this.id,
      date: this.date,
      user: this.User.getResponse(),
      description: this.description,
      price: this.price,
      settled: this.settled,
    };
  }
}
