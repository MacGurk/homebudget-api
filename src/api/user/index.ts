import { Router } from 'express';
import list from './list';
import create from './create';

export interface UserResponse {
  id: string;
  username: string;
  email: string;
}

export default Router().get('/', list).post('/', create);
