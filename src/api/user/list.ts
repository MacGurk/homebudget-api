import { Router, Response, Request } from 'express';
import { UserResponse } from './index';
import UserRepository from '../../db/repository/UserRepository';

export default Router().get('/', async (req: Request, res: Response<UserResponse[]>) => {
  const users = await UserRepository.findAll();

  res.send(users.map((user) => user.getResponse()));
});