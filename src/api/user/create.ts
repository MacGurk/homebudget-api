import { Request, Response, Router } from 'express';
import { UserResponse } from './index';
import UserRepository from '../../db/repository/UserRepository';

interface Error {
  message: string;
}

interface CreateRequest {
  username: string;
  email: string;
  password: string;
}

type CreateResponse = UserResponse | Error;

export default Router().post('/', async (req: Request, res: Response<CreateResponse>) => {
  const { username, email, password } = req.body as CreateRequest;

  if (await UserRepository.existsByUsername(username)) {
    res.status(400).send({ message: 'Username is already in use' });
  }
  if (await UserRepository.existsByEmail(email)) {
    res.status(400).send({ message: 'Email is already in use' });
  }

  const user = await UserRepository.create(username, email, password);

  res.send(user.getResponse());
});
