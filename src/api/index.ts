import { Router } from 'express';
import transaction from './transaction';
import user from './user';
import auth from './auth';
import jwtAuth from '../middleware/jwtAuth';

export default Router()
    .use('/user', jwtAuth, user)
    .use('/transaction', jwtAuth, transaction)
    .use('/login', auth);
