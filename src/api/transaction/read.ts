import { Request, Response, Router } from 'express';
import { TransactionResponse } from './index';
import TransactionRepository from '../../db/repository/TransactionRepository';

export default Router().get('/:transactionid', async (req: Request, res: Response<TransactionResponse>) => {
  const transaction = await TransactionRepository.findById(req.params.transactionid);

  res.send(transaction.getResponse());
});
