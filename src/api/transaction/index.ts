import { Router } from 'express';
import list from './list';
import create from './create';
import deleteTransaction from './deleteTransaction';
import read from './read';
import update from './update';

export interface TransactionResponse {
  id: string;
  date: Date;
  user: {
    id: string;
    username: string;
    email: string;
  };
  description: string;
  price: number;
  settled: boolean;
}

export default Router()
  .get('/', list)
  .get('/:transactionid', read)
  .post('/', create)
  .put('/:transactionid', update)
  .delete('/:transactionid', deleteTransaction);
