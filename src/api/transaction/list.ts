import { Request, Response, Router } from 'express';
import TransactionRepository from '../../db/repository/TransactionRepository';
import { TransactionResponse } from './index';

export default Router().get('/', async (req: Request, res: Response<TransactionResponse[]>) => {
  const transactions = await TransactionRepository.findAll();

  res.send(transactions.map((transaction) => transaction.getResponse()));
});
