import { Request, Response, Router } from 'express';
import { TransactionResponse } from './index';
import TransactionRepository from '../../db/repository/TransactionRepository';

interface CreateTransactionRequest {
  date: Date;
  userId: string;
  description: string;
  price: number;
}

export default Router().post('/', async (req: Request, res: Response<TransactionResponse>) => {
  const { date, userId, description, price } = req.body as CreateTransactionRequest;

  const transaction = await TransactionRepository.create(date, description, price, userId);

  res.send(transaction.getResponse());
});
